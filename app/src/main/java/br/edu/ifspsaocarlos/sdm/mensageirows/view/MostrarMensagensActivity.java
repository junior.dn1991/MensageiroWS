package br.edu.ifspsaocarlos.sdm.mensageirows.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import br.edu.ifspsaocarlos.sdm.mensageirows.R;
import br.edu.ifspsaocarlos.sdm.mensageirows.adapter.MensagensAdapter;
import br.edu.ifspsaocarlos.sdm.mensageirows.api.MensageiroApi;
import br.edu.ifspsaocarlos.sdm.mensageirows.model.Mensagem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MostrarMensagensActivity extends AppCompatActivity {
    private ListView mostrarMensagensLv;
    private Gson gson;
    private Retrofit retrofit;
    private MensageiroApi mensageiroApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_mensagens);
        mostrarMensagensLv = findViewById(R.id.lv_mostrar_mensagens);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setLenient();
        gson = gsonBuilder.create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(getString(R.string.url_base));
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        retrofit = builder.build();
        mensageiroApi = retrofit.create(MensageiroApi.class);

        Intent intent = getIntent();

        String origem = intent.getStringExtra("ORIGEM");
        String destino = intent.getStringExtra("DESTINO");
        String ultimaMensagem = intent.getStringExtra("ULTIMA_MENSAGEM") ;

        Call<List<Mensagem>> listaMensagensCall =
                mensageiroApi.getMensagems(ultimaMensagem,
                        origem,
                        destino);

        listaMensagensCall.enqueue(new Callback<List<Mensagem>>() {
            @Override
            public void onResponse(Call<List<Mensagem>> call, Response<List<Mensagem>>
                    response) {
                List<Mensagem> listaMensagens = response.body();

                MensagensAdapter adapter =
                        new MensagensAdapter(listaMensagens, MostrarMensagensActivity.this);
                mostrarMensagensLv.setAdapter(adapter);

            }
            @Override
            public void onFailure(Call<List<Mensagem>> call, Throwable t) {
                Toast.makeText(MostrarMensagensActivity.this, "Erro na recuperação das mensagens!",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}
