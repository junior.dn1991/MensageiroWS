package br.edu.ifspsaocarlos.sdm.mensageirows.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.edu.ifspsaocarlos.sdm.mensageirows.R;
import br.edu.ifspsaocarlos.sdm.mensageirows.model.Mensagem;

public class MensagensAdapter extends BaseAdapter {
    private final List<Mensagem> mensagens;
    private final Activity activity;

    public MensagensAdapter(List<Mensagem> mensagens, Activity activity) {
        this.mensagens = mensagens;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return mensagens.size();
    }

    @Override
    public Object getItem(int position) {
        return mensagens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(mensagens.get(position).getId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater()
                .inflate(R.layout.activity_lista_mensagens, parent, false);
        Mensagem mensagem = mensagens.get(position);

        TextView origem = (TextView)
                view.findViewById(R.id.activity_lista_mensagens_origem);
        TextView destino = (TextView)
                view.findViewById(R.id.activity_lista_mensagens_destino);
        TextView corpoMensagem = (TextView)
                view.findViewById(R.id.activity_lista_mensagens_corpo);

        origem.setText(mensagem.getOrigem().getNomeCompleto());
        destino.setText(mensagem.getDestino().getNomeCompleto());
        corpoMensagem.setText(mensagem.getCorpo());

        return view;
    }
}