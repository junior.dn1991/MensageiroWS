package br.edu.ifspsaocarlos.sdm.mensageirows.api;

import java.util.List;

import br.edu.ifspsaocarlos.sdm.mensageirows.model.Contato;
import br.edu.ifspsaocarlos.sdm.mensageirows.model.Mensagem;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface MensageiroApi {
    @POST("contato")
    Call<ResponseBody> postContato(@Body RequestBody novoContato);
    @POST("mensagem")
    Call<ResponseBody> postMensagem(@Body RequestBody novoContato);
    @GET("rawmensagens/{ultimaMensagemId}/{origemId}/{destinoId}")
    Call<List<Mensagem>> getMensagems(@Path("ultimaMensagemId") String ultimaMensagemId,
                                      @Path("origemId") String origemId, @Path("destinoId") String destinoId);
    @GET("rawcontatos")
    Call<List<Contato>> getContatos();

}
