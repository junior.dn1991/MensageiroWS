package br.edu.ifspsaocarlos.sdm.mensageirows.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import br.edu.ifspsaocarlos.sdm.mensageirows.R;

public class LerActivity extends AppCompatActivity {
    private EditText origemEt;
    private EditText destinoEt;
    private EditText ultimaMensagemEt;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ler);
        origemEt = findViewById(R.id.et_origem_ler);
        destinoEt = findViewById(R.id.et_destino_ler);
        ultimaMensagemEt = findViewById(R.id.et_ultima_mensagem_ler);
    }
    public void onClick(View v) {
        if (v.getId() == R.id.bt_ler){
            Intent mostraMensagensIntent = new Intent(LerActivity.this,
                    MostrarMensagensActivity.class);

            mostraMensagensIntent.putExtra("ORIGEM", origemEt.getText().toString());
            mostraMensagensIntent.putExtra("DESTINO", destinoEt.getText().toString());
            mostraMensagensIntent.putExtra("ULTIMA_MENSAGEM", ultimaMensagemEt.getText().toString());
            startActivity(mostraMensagensIntent);
        }
    }
}
