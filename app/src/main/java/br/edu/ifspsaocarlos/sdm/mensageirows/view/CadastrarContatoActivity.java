package br.edu.ifspsaocarlos.sdm.mensageirows.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import br.edu.ifspsaocarlos.sdm.mensageirows.R;
import br.edu.ifspsaocarlos.sdm.mensageirows.api.MensageiroApi;
import br.edu.ifspsaocarlos.sdm.mensageirows.model.Contato;
import br.edu.ifspsaocarlos.sdm.mensageirows.model.Mensagem;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CadastrarContatoActivity extends AppCompatActivity {
    private EditText nomeContatoEt;
    private EditText nicknameContatoEt;
    private Button adicionaContatoBt;
    private Gson gson;
    private Retrofit retrofit;
    private MensageiroApi mensageiroApi;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_contato);
        nomeContatoEt = findViewById(R.id.et_contato_name);
        nicknameContatoEt = findViewById(R.id.et_contato_nickname);
        adicionaContatoBt = findViewById(R.id.bt_cadastrar_novo_contato);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setLenient();
        gson = gsonBuilder.create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(getString(R.string.url_base));
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        retrofit = builder.build();
        mensageiroApi = retrofit.create(MensageiroApi.class);
    }

    public void cadastrarNovoContato(View v) {
        Contato contato = new Contato();
        contato.setNomeCompleto(nomeContatoEt.getText().toString());
        contato.setApelido(nicknameContatoEt.getText().toString());

        RequestBody contatoRb =
                RequestBody.create(MediaType.parse("application/json"), gson.toJson(contato));

        mensageiroApi.postContato(contatoRb).enqueue(
                new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {
                        Toast.makeText(CadastrarContatoActivity.this, "Contato cadastrado!",
                                Toast.LENGTH_SHORT).show();
                        limparCampos();
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(CadastrarContatoActivity.this, "Erro no cadastro do usuario!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    private void limparCampos(){
        nomeContatoEt.setText("");
        nicknameContatoEt.setText("");
    }
}
